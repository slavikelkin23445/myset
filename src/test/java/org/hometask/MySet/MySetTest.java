package org.hometask.MySet;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class MySetTest {

    @Test

    public void toArrayTest(){
        Set<Integer> i = new MySet<>();
        i.add(200);
        i.add(216);
        i.add(56);
        i.add(56);
        i.add(6);
        i.add(5);
        i.add(6);
        i.add(5);

        assertArrayEquals(new Integer[]{5,6,200,216,56}, i.toArray());
    }

    @Test
    public void RemoveAllTest(){
        Set<Integer> i = new MySet<Integer>();
        i.add(10);
        i.add(20);
        i.add(50);
        i.add(30);
        i.add(40);

        Set<Integer> i2 = new MySet<Integer>();
        i2.add(20);
        i2.add(10);
        i2.add(40);
        i2.add(30);

        assertTrue(i.removeAll(i2));

        assertArrayEquals(new Integer[]{50}, i.toArray());
    }

    @Test
    public void ContainAllTest(){
        Set<Integer> i = new MySet<Integer>();
        i.add(10);
        i.add(20);
        i.add(30);
        i.add(40);

        Set<Integer> i2 = new MySet<Integer>();
        i2.add(20);
        i2.add(10);
        i2.add(40);
        i2.add(30);

        assertTrue(i.containsAll(i2));
    }

    @Test
    public void AddAllTest(){
        Set<Integer> i = new MySet<>();
        i.add(200);
        i.add(216);
        i.add(56);
        i.add(56);
        i.add(6);
        i.add(5);
        i.add(6);
        i.add(5);

        Set<Integer> i2 = new MySet<Integer>();
        i2.add(20);
        i2.add(60);
        i2.add(70);
        i2.add(10);


        i.addAll(i2);

        assertArrayEquals(new Integer[]{20, 5, 6, 70, 200, 216, 56, 10, 60}, i.toArray());
    }

}
package org.hometask.MySet;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.*;

public class MySet<T> implements Set<T> {

    private static final int DEFAULT_CAPACITY = 16;
    private final Node[] arr;
    private int size = 0;

    @Data
    @Accessors(chain = true)
    private static class Node<T>{
        T value;
        Node<T> next;
    }

    public MySet(){
        this(DEFAULT_CAPACITY);
    }

    public MySet(int capacity){
        this.arr = new Node[capacity];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    private int calcIndex(Object o){
        return o.hashCode() % arr.length;
    }

    @Override
    public boolean contains(Object o) {
        int i = calcIndex(o);
        if(arr[i] == null) return false;

        for(Node<T> cur = arr[i]; cur != null; cur = cur.next){
            if(cur.value.equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int index = 0;
            private Node<T> cur;
            @Override
            public boolean hasNext() {
                return index < arr.length;
            }

            @Override
            public T next() {
                while(arr[index] == null) {
                    index++;
                    if (hasNext()) {
                        cur = arr[index];
                    }else{
                        break;
                    }
                }

                if(cur == null){
                    return null;
                }

                T val = cur.value;
                if(cur.next != null) {
                    cur = cur.next;
                }
                else{
                    cur = arr[++index];
                }
                return val;
            }
        };
    }

    @Override
    public Object[] toArray() {
        return toArray(new Object[0]);
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        a = Arrays.copyOf(a, size);
        int i = 0;
        for (T t : this) {
            if(t != null) a[i++] = (T1)t;
        }
        return a;
    }

    @Override
    public boolean add(T t) {
        int i = calcIndex(t);

        if(arr[i] == null){
            arr[i] = new Node<T>();
            arr[i].value = t;
            size++;
            return true;
        }

        for(Node<T> cur = arr[i]; cur != null; cur = cur.next){
            if(cur.value.equals(t)){
                return false;
            }
            else if(cur.next == null){
                cur.next = new Node<T>();
                cur.next.value = t;
                size++;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        int i = calcIndex(o);
        if(arr[i] == null)return false;

        Node<T> cur = arr[i];

        if(arr[i].next == null){
            cur.value = null;
            size--;
            return true;
        }

        for(; !cur.value.equals(o); cur = cur.next);

        if(cur.next != null){
            cur.setValue(cur.next.value).setNext(cur.next.next);
            size--;
            return true;
        }
        else{
            cur = arr[i];
            while(cur.next.next != null){
                cur = cur.next;
            }
            cur.next = null;
            size--;
            return true;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object el:c) {
            if (el != null) {
                if (contains(el)) {
                    continue;
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        for (Object t : c){
            if(t != null) add((T)t);
        }
        return true;
    }

    public boolean containsListItem(T val, Collection<?> c){
        for (Object el : c) {
            if (Objects.equals(val, el)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        for(Object i: this){
            if(i != null) {
                if (containsListItem((T)i, c)){
                    continue;
                }
                else {
                    remove(i);
                }
            }
        }
        return true;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        for (Object el: c) {
            if(el != null) {
            remove(el);
            }
        }
        return true;
    }

    @Override
    public void clear() {
        for(T t : this){
            if(t!= null) arr[calcIndex(t)] = null;
        }
    }
}